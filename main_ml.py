"""
This is a sample code that ML Team member will run.

Objective of this is to:

1. Develop and Test a Simple Linear Regression Model
2. Connect to MLFlow server and log the developed model
and associated parameters
3. Optionally register the model (if all parties are happy
for the model to be in Stage/Production

NOTE: This is not an application server,
"""


from os import environ
import numpy as np
from sklearn import datasets

from sklearn.metrics import mean_squared_error, r2_score

from mlflow import (
    MlflowClient,
    create_experiment,
    register_model,
    get_experiment_by_name,
    set_tracking_uri,
    set_registry_uri,
    start_run
)
from mlflow.pyfunc import log_model

from edgml.algo import (
    SimpleLinearRegression,
    linear_regression_model_info as model_info
)


EXPERIMENT_NAME = "DIABETES"
RUN_NAME = "LINEAR_REGRESSION_MODEL"
DESIRED_STAGE = "Staging"

MLFLOW_TRACKING_URI = environ.get("MLFLOW_TRACKING_URI", "http://localhost:5001")
MLFLOW_REGISTRY_URI = environ.get("MLFLOW_TRACKING_URI", "http://localhost:5001")


def develop_linear_regression_model():

    set_tracking_uri(MLFLOW_TRACKING_URI)
    set_registry_uri(MLFLOW_REGISTRY_URI)

    experiment = get_experiment_by_name(EXPERIMENT_NAME)
    if experiment:
        experiment_id = experiment.experiment_id
    else:
        experiment_id = create_experiment(EXPERIMENT_NAME)

    print(
        f"Experiment Name/ID in MLFLOW Server : {EXPERIMENT_NAME}/{experiment_id}")

    with start_run(
        experiment_id=experiment_id,
        run_name=model_info["run_name"]
    ) as current_run:

        current_run_id = current_run.info.run_id

        # Load the diabetes dataset
        diabetes_X, diabetes_y = datasets.load_diabetes(return_X_y=True)

        # Use only one feature
        diabetes_X = diabetes_X[:, np.newaxis, 2]

        # Split the data into training/testing sets
        diabetes_X_train = diabetes_X[:-20]
        diabetes_X_test = diabetes_X[-20:]

        # Split the targets into training/testing sets
        diabetes_y_train = diabetes_y[:-20]
        diabetes_y_test = diabetes_y[-20:]

        model = SimpleLinearRegression(iterations=10, lr=0.01, verbose=True)
        model.fit(diabetes_X_train, diabetes_y_train)

        predicted_y = model.predict(None, diabetes_X_test)

        mse = mean_squared_error(diabetes_y_test, predicted_y)
        r2 = r2_score(diabetes_y_test, predicted_y)

        # at this stage, we can log any parameter, results to MLFlow server
        # these parameters can include init_weights,
        print(f"Model Performance; MSE {mse}, R2 Score: {r2}")

        model_data = log_model(
            model_info["artifact_path"],
            python_model=model,
            code_path=[
                model_info["code_path"]
            ],
            signature=model_info["model_signature"]
        )

        print(f"Model Data --- {model_data}")

        registered_model = register_model(
            f"runs:/{current_run_id}/{model_info['artifact_path']}",
            name=model_info["registered_model_name"]
        )

        # Transitioning the model to Staging/Production is a model
        # governance issue where both ML-Team and App-Team need to
        # decide which model is worth transitioning.
        # This following part can be done separately.
        # here we are assuming the model is good enough for Staging

        print(f"Registration --- {registered_model}")

        mlflow_client = MlflowClient(
            tracking_uri=MLFLOW_TRACKING_URI,
            registry_uri=MLFLOW_REGISTRY_URI)
        mlflow_client.transition_model_version_stage(
            model_info["registered_model_name"],
            registered_model.version,
            archive_existing_versions=True,
            stage=DESIRED_STAGE
        )

if __name__ == "__main__":

    develop_linear_regression_model()