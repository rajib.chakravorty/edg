
from numpy import dtype, float32
from mlflow.models import ModelSignature
from mlflow.types import TensorSpec, Schema

input_schema = Schema(
    [
        TensorSpec(
            dtype(float32),
            (-1, 1)
       ),
    ]
)

model_signature = ModelSignature(input_schema)


linear_regression_model_info = {
    "artifact_path": "linear_regression_artifactory",
    "registered_model_name": "linear_regression_model",
    "code_path": "edgml",
    "model_signature": model_signature,
    "run_name": "Simple-Linear-Regression"
}