from numpy import dot, sum
from numpy.random import normal

from mlflow.pyfunc import PythonModel


class SimpleLinearRegression(PythonModel):
    def __init__(self, iterations=15000, lr=0.1, verbose=True):

        self.iterations = iterations # number of iterations the fit method will be called
        self.lr = lr # The learning rate
        self.losses = [] # A list to hold the history of the calculated losses
        self.W, self.b = None, None # the slope and the intercept of the model
        self.verbose = verbose

        if self.iterations < 1:
            print(f"Iterations must be at least 1. Found {iterations}. Using 1")
            self.iterations = 1

    def __loss(self, y, y_hat):
        """
        :param y: the actual output on the training set
        :param y_hat: the predicted output on the training set
        :return:
            loss: the sum of squared error

        """
        loss = dot((y-y_hat).T, (y-y_hat))
        self.losses.append(loss)
        return loss

    def __init_weights(self, X):
        """
        :param X: Training data
        """
        feature_length = X.shape[1]
        weights = normal(size=feature_length + 1)
        self.W = weights[:-1].reshape(-1, feature_length)
        self.b = weights[-1].reshape((-1,))

    def __sgd(self, X, y, y_hat):
        """

        :param X: The training feature
        :param y: The training target
        :param y_hat: The predicted target
        :return:
            sets updated W and b to the instance Object (self)
        """
        factor = 2./ X.shape[0]
        dW = factor * dot(X.T, (y-y_hat))
        db = factor * sum((y-y_hat), axis=0)
        self.W = self.W - (self.lr * dW)
        self.b = self.b - (self.lr * db)

    def fit(self, X, y):
        """
        :param X: The training set
        :param y: The true output of the training set
        :return:
        """
        y = y.reshape((-1, 1))
        self.__init_weights(X)
        y_hat = self.predict(None, X)
        loss = self.__loss(y, y_hat)
        if self.verbose:
            print(f"Initial Loss: {loss}")
        for i in range(self.iterations + 1):
            self.__sgd(X, y, y_hat)
            y_hat = self.predict(None, X)
            loss = self.__loss(y, y_hat)
            if self.verbose:
                if not i % 100:
                    print(f"Iteration {i}, Loss: {loss}")

    def predict(self, _, X):
        """
        :param X: The training dataset

        :return:
            y_hat: the predicted output

        Note: Ignored input _ is for MLFlowServer context argument
        """

        y_hat = X * self.W + self.b
        return y_hat
