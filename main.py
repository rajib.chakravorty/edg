from os import environ
from flask import Flask, request, Response, make_response
from numpy import array, float32
from mlflow import set_tracking_uri, set_registry_uri
from mlflow.pyfunc import load_model

app = Flask(__name__)

MLFLOW_TRACKING_URI = environ.get("MLFLOW_TRACKING_URI", "http://localhost:5001")
MLFLOW_REGISTRY_URI = environ.get("MLFLOW_TRACKING_URI", "http://localhost:5001")

# these may come from a separate services - e.g. secrets manager,
# AppConfig etc.
MODEL_NAME = "linear_regression_model"
MODEL_VERSION = "1"
ML_MODEL_REFERENCE = f"models:/{MODEL_NAME}/{MODEL_VERSION}"

set_tracking_uri(MLFLOW_TRACKING_URI)
set_registry_uri(MLFLOW_REGISTRY_URI)

loaded_model = load_model(ML_MODEL_REFERENCE)


@app.route('/stream', methods=['POST'])
def stream_process():
    args = request.get_json(force=True)
    data = array(args["data"]).reshape((-1, 1)).astype(float32)
    if data.shape[0] > 1:
        response = {
            "error": "Found more than one data point for prediction; Use /batch endpoint."
        }
        return response

    response = {
        "outcome": loaded_model.predict(data).tolist()
    }
    return response


@app.route('/batch', methods=['POST'])
def batch_process():
    args = request.get_json(force=True)
    data = array(args["data"]).reshape((-1, 1)).astype(float32)
    response = {
        "outcome": loaded_model.predict(data).tolist()
    }
    return response


if __name__ == '__main__':

    print('Running HTTP')
    app.run('0.0.0.0', port=9000)