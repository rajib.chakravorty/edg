#!/bin/bash

mlflow server --backend-store-uri sqlite:///./mlflow-backend.db --registry-store-uri sqlite:///./mlflow-backend.db --default-artifact-root ./mlflow-models --host 0.0.0.0 --port 5001 &