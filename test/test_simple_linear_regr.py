from unittest import TestCase
from numpy import array
from numpy.random import seed, random

import matplotlib.pyplot as plt

from sklearn.metrics import mean_squared_error, r2_score
from edgml.algo import generate_data, SimpleLinearRegression


class TestSimpleLinearRegrAlgo(TestCase):

    def test_model_class(self):

        seed(1234)
        model = SimpleLinearRegression(iterations=4, lr=0.01, verbose=False)

        assert model.lr == 0.01
        assert model.iterations == 4
        assert model.verbose is False
        self.assertCountEqual(model.losses, [])
        assert model.W is None
        assert model.b is None

        model.fit(
            array([1, 2, 3]).reshape((-1, 1)),
            array([2, 4, 6]).reshape((-1, 1)))

        assert len(model.losses) == 6

        loss_sizes = [len(model.losses[loss_idx]) for loss_idx, _ in enumerate(model.losses)]
        self.assertCountEqual(loss_sizes, [1] * 6)

        model = SimpleLinearRegression(iterations=-1, lr=0.01, verbose=False)
        assert model.iterations == 1

