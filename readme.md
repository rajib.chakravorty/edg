## Background
Objective of this exercise are:

1. to develop and demonstrate a model architecture that merges Machine Learning exercises
with application development using MLOps for a scalable, sustainable and maintable
machine learning based applications.
2. Demonstrate how a CI/CD pipeline can be used in such an integrated development and
production enviroment


## What is NOT achieved

1. Containerization [This is fairly straight forward to achieve given how the
architecture is set up in the challenge. However, that lacks MLOps in the middle
between the ML Code and Application Code. Ideally the application code goes 
through the CI/CD while ML Code interacts with MLFLow/Vertex and other similar
MLOps services. Given that this coding has MLFlow component, the dockerization
has an external dependency, e.g. MLServer running. Therefore containerization
will not serve its intended purpose.]

2. Rigourous testing [A complete rigourous unit+integration testing involves
time and careful timing. I have included one test as a demonstration purpose. However,
this is nowhere near to a proper product ready code.]

3. Maintainable code [Proper maintainable code would require much more docoumentation
and testing. With limited time, this is hard to achieve. However, I believe someone
with similar level of expertise would be able to follow the code.]


## What is Achieved

1. Demonstration of MLOps based machine learning application.
A draft architecture diagram (image below) separates the Machine learning developement
and application development. These two are connected by MLFlow/Vertex/Other MLOps services.
ML team can run ML experiment and logs the result to MLOps server. Application and ML team
together can govern the models for testing or production. An Application CI/CD
can pull model and code together for deployment.

<img src=assets/ml_application_archi.png/>

2. System Running end-to-end

With the correct environment set up and MLFlow server running, the system will run
end-to-end and deliver a result via 2 API end point on application server (
`/batch` and `/stream`).


## Running the project (Step-by-Step)

## For both Machine Learning Team and Application Team
1. Install and set up conda environment (instruction [here](https://docs.conda.io/en/main/miniconda.html)).
2. Create and activate an enviroment

```commandline
$ conda create -n edg-env python=3.9
$ conda activate edg-env
```

3. Clone the project from git. (Let the cloned directory be referred to as $REPO)
4. For development purpose, 

```commandline
$ pip install -r dev_requirements.txt
```

For production purpose
```commandline
$ pip install dev_requirements.txt
```

5. Install the `edgml` package

```commandline
$ pip install .
```

6. Run tests [applicable in development/tests]

```commandline
$ pytest tests/test_**
```

7. Run the MLFlow server 

In a separate terminal, run

```commandline
$ ./run_mlflow_server.sh
```
NOTE: This is a local SQLite based MLFlow server.

### Machine learning Team

8. Develop, Log and Register the model

In another terminal and with `edg-ml` environment active, execute

```commandline
$ python main_ml.py
```

NOTE: A ML team will run the experiment many times, tune the parameters and may
or may not log all the models/artifacts/metrics etc. Moreover, ML team and application team will 
decide the models for testing and production. This is just a demonstration
of one run containing everything. 

### Application Team

8. In another terminal and with `edg-ml` environment active, execute

```commandline
$ python main.py
```

This will start a service on port `9000` and will expose two end points

1. `/batch`: accepts a `json` with format `{"data": [X_1,X2]}` input and reports
and output json with format `{"outcome":[[Y_1], [Y_2]]}`.
2. `/stream`: accepts a `json` with format `{"data": [X_1]}` input and reports
and output json with format `{"outcome":[[Y_1]]}`.

NOTE: Supplying more than one input to `/stream` will return an error

`{"error": "Found more than one data point for prediction; Use /batch endpoint."}`

However, supplying just one input to the `/batch` will not product any error.

### Consuming the service

1. Batch end point

```commandline
curl -X POST http://localhost:9000/batch -H "Content-Type: application/json" -d '{"data": [32.42, 35.45]}'
```
will produce following output
```
{"outcome":[[-60.75864529902506],[-63.04347368469834]]}
```

2. Stream end point
```commandline
curl -X POST http://localhost:9000/stream -H "Content-Type: application/json" -d '{"data": [32.42]}'
```
will produce the following output
```commandline
{"outcome":[[-60.75864529902506]]}
```

3. Trying to use Stream end point with more than one input
```commandline
curl -X POST http://localhost:9000/stream -H "Content-Type: application/json" -d '{"data": [32.42, 35.45]}'
```
will cause following error message to be returned
```commandline
$ {"error":"Found more than one data point for prediction; Use /batch endpoint."}
```

## Specific Question and Answer

Does your solution work end to end?
- Yes (With all dependencies are suitable met)

Are there any unit tests or integration tests?
- Minimal (Production quality code will require much more time and careful planning for tests. I have added one sample unit test for demonstration only).

Has security/monitoring been considered?
- Security is an overall architecture and system design issue (schema/table/view/data level access control, authorization, access token).
While all of these can be a part of a complete production level architecture, I do not consider this implementation anywhere near that level.

How is your solution documented? Is it easy to build on and for other developers to understand
- While a sufficiently familiar ML Application developer will build and run the current implementation, I will consider this implementation lacking
for a full production level codebase.

How performant is your solution both from a code perspective and a scalability perspective as a service
- There are many issues that need resolving before I can consider this scalable. Some of these are:

  a. Size of data (e.g. mini-batching instead of full batching, using distributed learning for bigger data size)
  b. Framework for hyper-parameter tuning (and doing that in a scalable way)
  c. On the application side, the server by itself is not scalable. Either a dockerization+Kubernetes or AWS Lambda implementation would improve.
 
Has due consideration been given to what a production ML system would require? This could be interactions or dependencies with other systems.

- This is met within the limitation of how the challenge is setup.
