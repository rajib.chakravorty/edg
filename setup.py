from setuptools import setup

version = '0.0.1'

setup(
    name='edgml',
    version=version,
    description='EDG Linear Regression',
    author='RChakravorty',
    packages=[
        'edgml',
        'edgml.algo'
    ]
)